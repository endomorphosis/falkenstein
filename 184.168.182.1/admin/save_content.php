<?
include_once('../includes/dbcon.php');
include_once('../includes/FAL_class.php');
$FAL = new FAL();
$FAL->id = $_REQUEST["id"]; 
$FAL->page = $_REQUEST["page"]; 
$FAL->content = $_REQUEST["content"]; 
$FAL->savepagecontent();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Falkenstein.com - Content Manager</title>

<script type="text/javascript" src="ckeditor/ckeditor.js"></script>

<link href="css/cm.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div align="center"> <a href="index.php"><img src="img/cm_header.gif" width="917" height="133" border="0" /></a>
<form action="save_content.php" method="post">
		<p>&nbsp;</p>
		<p><strong>Page content  saved successfully.</strong></p>
		<p>&nbsp;</p>
		<p><strong><a href="index.php">Continue &gt;&gt;</a></strong></p>
		  <p>
		    <script type="text/javascript">
			//<![CDATA[

				// This call can be placed at any point after the
				// <textarea>, or inside a <head><script> in a
				// window.onload event handler.

				// Replace the <textarea id="editor"> with an CKEditor
				// instance, using default configurations.
				CKEDITOR.replace( 'content' );

			//]]>
			</script>
    </p>
	    <p>&nbsp;</p>
</form>
</div>
</body>
</html>

<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); 
$page_title = get_post_meta($post->ID, 'mediac_title', true);
$page_desc = get_post_meta($post->ID, 'mediac_desc', true);
?>
<!-- start wrapper -->
<div id="wrapper">
<div id="header"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/header1.png" width="960" height="134" border="0" usemap="#HeaderMap" /></div>
<?PHP $dir=get_stylesheet_directory(); include($dir."/inc-header.php"); ?>    
<div id="interior"> 
<h1><?php echo get_the_title(); ?> </h1>
<!-loop starts--> <?php
			/* Run the loop to output the posts.
			 * If you want to overload this in a child theme then include a file
			 * called loop-index.php and that will be used instead.
			 */
			 get_template_part( 'loop', 'index' );
			?>
<!-loop ends-->
</div>
<div id="interior_right">
<?php get_sidebar(); ?>
</div>
<?PHP $dir=get_stylesheet_directory(); include($dir."/inc-footer-int.php"); ?>
</body>
</html>

<?php

/*

Template Name: pageHomeRev1

*/

?>

<?php get_header(); 

$page_title = get_post_meta($post->ID, 'mediac_title', true);

$page_desc = get_post_meta($post->ID, 'mediac_desc', true);

?>

<!-- start wrapper -->

<div id="wrapper">

<div id="header"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/banner.png" width="960" height="134" border="0" usemap="#HeaderMap" alt = "home page banner" /></div>

<?PHP $dir=get_stylesheet_directory(); include($dir. "/inc-header.php"); ?>

<div id="homebanner"><img src="http://falkenstein.com/wp-content/uploads/2014/04/banner.png" width="637" height="264"  alt="homepage image" /><!--<div id="homebanner"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/banner.png" width="637" height="264" alt="homepage image" />-->

<div id="homebannerHeadline"><?php get_sidebar('homebannertext') ?></div>

</div>

<div id="homeright">

<?php get_sidebar('homeright') ?>

</div><!-- end homeright --> 

	<?php if(($page_title) || ($page_desc)) {?>

            <?php if($page_title) { echo "<h1>".$page_title."</h1>"; } ?>            

            <?php if($page_desc) { ?><p class="intheader-paragraph"><?php echo $page_desc; ?></p><?php } ?>



      

    <?php } ?> 

<div class="clearboth"></div>      

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>                

            <?php the_content(); 

			wp_link_pages(array('before' => '<p><strong>'.__('Pages:', 'mediaconsult').'</strong> ', 'after' => '</p>')); 

            endwhile; endif; wp_reset_query(); ?>

<?PHP  $dir=get_stylesheet_directory(); include($dir."/inc-footer.php"); ?>

</body>

</html>
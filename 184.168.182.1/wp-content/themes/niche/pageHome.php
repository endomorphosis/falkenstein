<?php
/*
Template Name: pageHome
*/
?>
<?php get_header(); 
$page_title = get_post_meta($post->ID, 'mediac_title', true);
$page_desc = get_post_meta($post->ID, 'mediac_desc', true);
?>
<!-- start wrapper -->
<div id="wrap">
<div id="header"><img src="http://falkenstein.com/wp-content/uploads/2014/04/WebsiteBanner2.png" width="960" height="134" border="0" usemap="#HeaderMap" alt = "home page banner" />

</div><!-- end header -->

<?PHP $dir=get_stylesheet_directory(); include($dir. "/inc-header.php"); ?>

<!--<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/homebordertop.jpg" alt="border" /> --> 

<div id="homeMain">

<div id="homeleft">

   <div id="homebanner"><img src="http://falkenstein.com/wp-content/uploads/2014/04/WebsitePhoto3.png"  height="264" alt="homepage image" />

     <div id="homecontent">

<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/twolines.png"  alt="line" />

    <?php if(($page_title) || ($page_desc)) {?>

            <?php if($page_title) { echo "<h1>".$page_title."</h1>"; } ?>            

            <?php if($page_desc) { ?><p class="intheader-paragraph"><?php echo $page_desc; ?></p><?php } ?>  

    <?php } ?> 

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>                

            <?php the_content(); 

			wp_link_pages(array('before' => '<p><strong>'.__('Pages:', 'mediaconsult').'</strong> ', 'after' => '</p>')); 

            endwhile; endif; wp_reset_query(); ?>

    </div><!-- end homecontent --> 

    </div><!-- end homebanner --> 

</div><!-- end homeleft -->

   <div id="homeright"><div id="homertcontent">

    <?php get_sidebar('homeright') ?>

    </div><!-- end homerightcontent --> 

   </div><!-- end homeright --> 

<div class="clearboth"></div>

</div><!-- end homeMain -->

<?php get_sidebar('homebottom') ?>

<?PHP  $dir=get_stylesheet_directory(); include($dir."/inc-footer.php"); ?>

</body>

</html>
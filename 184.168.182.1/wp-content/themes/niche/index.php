<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
get_header(); 
$page_title = get_post_meta($post->ID, 'mediac_title', true);
$page_desc = get_post_meta($post->ID, 'mediac_desc', true);
?>
<!-- index.php start wrapper -->
<div id="wrap">
<?php include('pageTop.php'); ?>
<div id="container"><div id="container_row">
<div class="layout-cell interior"> 
<h1>Happenings</h1>
<!-- loop starts--> <?php
			/* Run the loop to output the posts.
			 * If you want to overload this in a child theme then include a file
			 * called loop-index.php and that will be used instead.
			 */
			 get_template_part( 'loop', 'index' );
			?>
<!-- loop ends-->
</div>
<div class="layout-cell interior_mid"></div>
<div class="layout-cell interior_right"> 
<?php get_sidebar(); ?>
</div> 
</div></div><!-- close container & container_row with left and right portions of page -->
<?PHP $dir=get_stylesheet_directory(); include($dir."/inc-footer-int.php"); ?>
</body>
</html>
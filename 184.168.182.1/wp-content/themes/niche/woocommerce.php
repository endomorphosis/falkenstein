<?php
/*
Template Name: pageHome
*/
?>
<?php get_header(); 
$page_title = get_post_meta($post->ID, 'mediac_title', true);
$page_desc = get_post_meta($post->ID, 'mediac_desc', true);
?>

<!-- start wrapper -->
<div id="wrap">

<?php
$postid = get_the_ID();
$myHeader = get_post_meta( $postid, 'custom_header', 'true' );
$displaycontact = get_post_meta( $postid, 'display_header_content', 'true' );
$dir = get_stylesheet_directory_uri();
//echo $myHeader;
if ($myHeader == '' || !$myHeader)
   { $headerImg = "$dir/img/header.png"; }
else
 {  $headerImg = $myHeader; }
//echo "Header"; echo $headerImg;
//echo "display"; echo $displaycontact
?>
<div id="header"><img src="<?php echo $headerImg; ?>" width="960" height="134" border="0" usemap="#HeaderMap" alt = "home page banner" />
<?php if ($displaycontact != 'false') { ?>
<div id="header-text"><a href="&#109;&#097;&#105;&#108;&#116;&#111;:&#100;&#114;&#110;&#105;&#099;&#104;&#101;&#064;&#102;&#097;&#108;&#107;&#101;&#110;&#115;&#116;&#101;&#105;&#110;&#046;&#099;&#111;&#109;">&#100;&#114;&#110;&#105;&#099;&#104;&#101;&#064;&#102;&#097;&#108;&#107;&#101;&#110;&#115;&#116;&#101;&#105;&#110;&#046;&#099;&#111;&#109;</a><br />
<span style="font-size:105%">503.781.0966</span></div>
</div> <?php } ?>
</div><!-- end header -->
<!-- end header -->
<?PHP $dir=get_stylesheet_directory(); include($dir."/inc-header.php"); ?> 


<?php //include('pageTop.php'); ?>
<div id="container"><div id="container_row">
<div class="layout-cell interior">  
  	<?php if(($page_title) || ($page_desc)) {?>
            <?php if($page_title) { echo "<h1>".$page_title."</h1>"; } ?>            
            <?php if($page_desc) { ?><p class="intheader-paragraph"><?php echo $page_desc; ?></p><?php } ?>
    <?php } ?>   
            <?php  ?>
              <?php woocommerce_content(); ?> HELLO WORLD
            <?php //the_content(); 		
			wp_link_pages(array('before' => '<p><strong>'.__('Pages:', 'mediaconsult').'</strong> ', 'after' => '</p>')); 
        			
       wp_reset_query(); ?>
</div> 
<div class="layout-cell interior_mid"></div>
<div class="layout-cell interior_right"> 
<?php get_sidebar(); ?>
</div> 
</div></div><!-- close container & container_row with left and right portions of page -->
<?PHP $dir=get_stylesheet_directory(); include($dir."/inc-footer-int.php"); ?>
</body>
</html>
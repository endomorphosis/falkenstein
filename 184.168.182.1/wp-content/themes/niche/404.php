<?php get_header(); 
$page_title = get_post_meta($post->ID, 'mediac_title', true);
$page_desc = get_post_meta($post->ID, 'mediac_desc', true);
?>
 <meta http-equiv="refresh" content="2; url=http://falkenstein.com" />
<!-- start wrapper -->
<div id="wrap">
<?php include('pageTop.php'); ?>
<div id="container"><div id="container_row">
<div class="layout-cell interior">  
<h2><?php _e('Error 404 - Page not Found', 'framework'); ?></h2>
        
		<p><h3><?php _e('The page you were looking for was not found on the server. We apologize for any inconvenience.', 'framework');?></h3></p>
                <p><h3><?php _e('You will be redirected to our Home Page at http://falkenstein.com', 'framework');?></h3></p>
</div> 
<div class="layout-cell interior_mid"></div>
<div class="layout-cell interior_right"> 
<?php get_sidebar(); ?>
</div> 
</div></div><!-- close container & container_row with left and right portions of page -->
<?PHP $dir=get_stylesheet_directory(); include($dir."/inc-footer-int.php"); ?>
</body>
</html>
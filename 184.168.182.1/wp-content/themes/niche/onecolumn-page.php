<?php
/*
Template Name: One column, no sidebar
*/
?>
<?php get_header(); 
$page_title = get_post_meta($post->ID, 'mediac_title', true);
$page_desc = get_post_meta($post->ID, 'mediac_desc', true);
?>
<?php get_header(); 
$page_title = get_post_meta($post->ID, 'mediac_title', true);
$page_desc = get_post_meta($post->ID, 'mediac_desc', true);
?>
<!-- start wrapper one column page-->
<div id="wrap">
<?php include('pageTop.php'); ?>
<div class="interior" style="width:880px;">  
	<?php if(($page_title) || ($page_desc)) {?>
            <?php if($page_title) { echo "<h1>".$page_title."</h1>"; } ?>            
            <?php if($page_desc) { ?><p class="intheader-paragraph"><?php echo $page_desc; ?></p><?php } ?>

      
    <?php } ?>     
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                
            <?php the_content(); 
			
			wp_link_pages(array('before' => '<p><strong>'.__('Pages:', 'mediaconsult').'</strong> ', 'after' => '</p>')); 
        			
            endwhile; endif; wp_reset_query(); ?>
</div>
<?PHP $dir=get_stylesheet_directory(); include($dir."/inc-footer-int.php"); ?>
</body>
</html>
<?php
/*
Template Name: pageHome
*/
?>
<?php get_header(); 
$page_title = get_post_meta($post->ID, 'mediac_title', true);
$page_desc = get_post_meta($post->ID, 'mediac_desc', true);
?>
<!-- start wrapper -->
<div id="wrap">
<?php include('pageTop.php'); ?>
<div id="container"><div id="container_row">
<div class="layout-cell interior">  
  	<?php if(($page_title) || ($page_desc)) {?>
            <?php if($page_title) { echo "<h1>".$page_title."</h1>"; } ?>            
            <?php if($page_desc) { ?><p class="intheader-paragraph"><?php echo $page_desc; ?></p><?php } ?>
    <?php } ?>   
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
              
            <?php //the_content(); 		
			wp_link_pages(array('before' => '<p><strong>'.__('Pages:', 'mediaconsult').'</strong> ', 'after' => '</p>')); 
        			
            endwhile; endif; wp_reset_query(); ?>
</div> 
<div class="layout-cell interior_mid"></div>
<div class="layout-cell interior_right"> 
<?php get_sidebar(); ?>
</div> 
</div></div><!-- close container & container_row with left and right portions of page -->
<?PHP $dir=get_stylesheet_directory(); include($dir."/inc-footer-int.php"); ?>
</body>
</html>
<?php 
	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
                 'primary' => __( 'Primary Menu', 'niche'),
		'secondary' => __( 'Secondary Navigation', 'niche' )
	) );

/**
 * Register widgetized areas.
 * http://wordpress.org/support/topic/twentyten-child-theme-adding-widget-areas-via-cgilds-functionsphp?replies=5
 */

// located on home page in box on right. Empty by default.
    register_sidebar( array(
		'name' => __( 'First Home Widget Area', 'niche' ),
		'id' => 'first-home-widget-area',
		'description' => __( 'The first home widget area', 'niche' ),
		//'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		//'after_widget' => '</li>',
		'before_title' => '<h1 class="widget-title">',
		'after_title' => '</h1>'
	) );	

// located on home page text that displays on banner image. Empty by default.
    register_sidebar( array(
		'name' => __( 'Home Banner Text Widget Area', 'niche' ),
		'id' => 'homebanner-widget-area',
		'description' => __( 'The text for home banner', 'niche' ),
	'before_widget' => ' ',
	'after_widget'  => '',
	'before_title'  => ' ',
	'after_title'   => ' ' 
	) );	


function twentyten_posted_on() {
	printf( __( '<span class="%1$s">Posted on</span> %2$s', 'twentyten' ),
		'meta-prep meta-prep-author',
		sprintf( '<a href="%1$s" title="%2$s" rel="bookmark"><span class="entry-date">%3$s</span></a>',
			get_permalink(),
			esc_attr( get_the_time() ),
			get_the_date()
		)
	);
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-type" content="text/html; charset=<?php bloginfo( 'charset' ); ?>" />
<meta name="robots" content="index, follow" />
<meta name="author" content="Mosaik web" />
<meta name="keywords" content="Niche,Dr. Lynda Falkenstein, Nichecraft,Niche Line,Niche Store, Focus, Business Niche, Personal Niche, Business Focus, Personal Focus, Prosperity, Consultant, Success, Career, Financial Advisor Niche, Graduate, Brand, Branding Change, Small business, Specialness, Unique, Niche Consultant, Jobs, Competition, Coaching, Alliance marketing" />
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" />
<?php $titleText="| Dr. Lynda Falkenstein |Nichecraft | Niche Store | Niche Consultant| Business Niche | Financial Advisor Niche"; ?> 
<?php $descText="Dr. Lynda  Falkenstein, aka The Niche Doctor, is an expert in the process of achieving focus and managing change, especially in volatile and rapidly altering environments. Her nine-step process successfully guides individuals through major life-transitions"; ?>
<title><?php if (is_home () )    {  echo 'Happenings'; echo $titleText; }  
         elseif (is_category() ) { single_cat_title();  echo $titleText; }
        elseif (is_single() ) { single_post_title();  echo $titleText; }
         elseif (is_page() ) {  single_post_title(); echo $titleText; }
         else { wp_title('',true); } ?>
</title>
<meta name="description" content="<?php if (is_home () )    { echo $descText; echo ' | Home Page';  }  
         elseif (is_category() ) { single_cat_title();  echo ' | ' ; echo $descText; }
        elseif (is_single() ) { single_post_title(); echo ' | ' ; echo $descText; }
        elseif (is_page() ) { single_post_title();echo ' | ' ; echo $descText; }
         else { wp_title('',true); } ?>" />
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />       
<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css' />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )

		wp_enqueue_script( 'comment-reply' );
	wp_head();
?>
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_stylesheet_directory_uri(); ?>/master.css" />
<!--[if IE]>  
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_stylesheet_directory_uri(); ?>/masterIE.css" />
<![endif]-->   
</head>
<body>
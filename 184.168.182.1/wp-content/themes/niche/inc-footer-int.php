<div class="clearboth"></div>
<div id="interior_footer"> 
  <table width="900">
    <tr>
      <td><div align="left"><b><a href="<?php echo get_site_url(); ?>/signup/">Newsletter Signup &gt;&gt;</a></b></div></td>
      <td><div align="left"><b><a href="<?php echo get_site_url(); ?>/newsletter-archives/">Newsletter Archives &gt;&gt;</a></b></div></td>
      <td width="125"><img class="social-int" src="http://falkenstein.com/img//facebook-int.gif" border="0" usemap="#SMmap" alt="facebook" /></td>
    </tr>
  </table>
</div>
<div id="redline3"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/redline1.png" width="960" height="7" alt="line" /></div>
<div id="footer">
<table class="menu"><tbody><tr><td class="menu"><?php wp_nav_menu( array( 'container_class' => 'menu-footer', 'theme_location' => 'secondary' ) ); ?></td></tr></tbody></table>
</div>
<!-- end Wrapper -->
</div>
<div id="copyright">Copyright &copy; <script type="text/javascript">var d = new Date();document.write(d.getFullYear());</script> Dr. Lynda Falkenstein. All rights reserved.<br />
<span style="font-size:90%;">a little website help from <a href="http://mosaikweb.com" target="_blank" ><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo_float_tiny.png" alt="Logo" /> Mosaik Web</a></span>		
 </div>
<br /><br />
<map name="SMmap" id="SMmap">
     <area shape="rect" coords="27,2,48,22" href="http://twitter.com/#!/drniche" target="_blank" />
  <area shape="rect" coords="3,4,20,21" href="https://www.facebook.com/TheNicheDoctor" target="_blank" />
  <area shape="rect" coords="54,3,71,19" href="http://www.linkedin.com/pub/dr-lynda-falkenstein-aka-%22the-niche-doctor%22/0/ba/2a7" target="_blank" />
</map>
<map name="HeaderMap" id="HeaderMap">
    <area shape="rect" coords="193,9,757,129" href="/"  alt="home" />
  </map>
   <map name="SocialMap" id="SocialMap">
    <area shape="rect" coords="34,11,61,37" href="https://www.facebook.com/TheNicheDoctor" target="_blank"  alt="fb" />
    <area shape="rect" coords="66,11,93,37" href="http://twitter.com/#!/drniche" target="_blank" alt="twitter" />
    <area shape="rect" coords="98,11,123,38" href="http://www.linkedin.com/pub/dr-lynda-falkenstein-aka-%22the-niche-doctor%22/0/ba/2a7" target="_blank" alt="linked in" />
</map>


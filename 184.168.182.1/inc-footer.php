<div class="clearboth"></div>


<div id="redline1"><img src="img/redline1.png" width="960" height="7" /></div>

<div id="footer">
<a href="home.php">Home</a>
<a href="drniche.php?page=About">About</a>
<a href="drniche.php?page=Services">Services</a>
<a href="http://drniche.mybigcommerce.com">Niche Store</a>
<a href="drniche.php?page=Happenings">Happenings</a>
<a href="drniche.php?page=Contact">Contact</a>
</div>

</div>
<div id="copyright">Copyright &copy; 2012 Dr. Lynda Falkenstein. All rights reserved.</div>
<br /><br />

<map name="HeaderMap" id="HeaderMap">
    <area shape="rect" coords="814,57,952,85" href="newsletter.php" />
    <area shape="rect" coords="193,9,757,129" href="index.php" />
  </map>
  
   <map name="SocialMap" id="SocialMap">
    <area shape="rect" coords="25,17,48,41" href="http://www.facebook.com/pages/The-Niche-Doctor-Dr-Lynda-Falkenstein/306713512739528" target="_blank" />
    <area shape="rect" coords="54,16,74,39" href="http://twitter.com/#!/graduateandgo" target="_blank" />
    <area shape="rect" coords="79,17,101,40" href="http://www.linkedin.com/pub/dr-lynda-falkenstein-aka-%22the-niche-doctor%22/0/ba/2a7" target="_blank" />
  </map>
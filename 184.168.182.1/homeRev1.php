<?
include_once('includes/dbcon.php');
include_once('includes/FAL_class.php');
$FAL = new FAL();
$FAL->page = $_REQUEST["page"]; 
?>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Dr. Lynda Falkenstein</title>
<link href="css/master.css" rel="stylesheet" type="text/css" />

<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
</head>

<body>


<!-- start wrapper -->
<div id="wrapper">

<?PHP include('inc-header.php'); ?>

<div id="homebanner"><img src="img/bannerOrig.png" width="637" height="264" /></div>

<div id="homeright"><h1>Niche News</h1>

<?PHP $FAL->getnews(); ?>

</div>

<div class="clearboth"></div>


<div class="hbox"><img src="img/about.png" width="292" height="26" />
<div class="box_text"><?PHP $FAL->getabout(); ?></div>
<div align="right"><a href="drniche.php?page=About">LEARN MORE ></a></div>
 </div>

<div class="hbox"><img src="img/shop.png" width="292" height="26" />
<div class="box_text"><?PHP $FAL->getshop(); ?></div>
<div align="right"><a href="http://drniche.mybigcommerce.com" target="_blank">VIEW STORE ></a></div>
</div>

<div class="hbox4"><img src="img/connect.png" width="292" height="26" />
<div class="box_text">
<ul><li>503.781.0966</li></ul>
<ul><li><a href="mailto:drniche@falkenstein.com">drniche@falkenstein.com</a></li></ul>
<div align="right"><img src="img/facebook.png" width="100" height="40" border="0" usemap="#SocialMap" />
 
  &nbsp;&nbsp;&nbsp;&nbsp;</div>

</div>

<!-- end wrapper -->
</div>

<?PHP include('inc-footer.php'); ?>
</body>
</html>

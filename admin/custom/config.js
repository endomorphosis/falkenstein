﻿/*
Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	config.width = '360';
	config.height = '120';
	config.language = 'en';
    config.uiColor = '#bdbdbd';
	config.toolbar = 'Full';
    config.contentsCss = 'http://www.falkenstein.com/css/master1.css';
	config.baseHref = 'http://www.falkenstein.com/';
	
};

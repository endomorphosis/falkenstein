<?
include_once('../includes/dbcon.php');
include_once('../includes/FAL_class.php');
$FAL = new FAL();
$FAL->page = $_REQUEST["page"]; 
$FAL->getpagecontent();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Falkenstein.com - Content Manager</title>

<script type="text/javascript" src="ckeditor/ckeditor.js"></script>

<link href="css/cm.css" rel="stylesheet" type="text/css" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>


</head>

<body>
<div align="center"> <a href="index.php"><img src="img/cm_header.gif" width="917" height="133" border="0" /></a>
<form action="save_content.php" method="post">
		<p>&nbsp;</p>
		<p><strong>Edit content for page: <?PHP echo $FAL->page_title; ?></strong></p>
		  <p>
		    <textarea cols="80" id="content" name="content" rows="10"><?PHP echo $FAL->content; ?>
		  </textarea>
    <script type="text/javascript">

			CKEDITOR.replace( 'content',
    {
		customConfig : 'http://www.falkenstein.com/admin/custom/config.js',
        filebrowserBrowseUrl : '/admin/ckfinder/ckfinder.html',
        filebrowserImageBrowseUrl : '/admin/ckfinder/ckfinder.html?Type=Images',
        filebrowserFlashBrowseUrl : '/admin/ckfinder/ckfinder.html?Type=Flash',
        filebrowserUploadUrl : '/admin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
        filebrowserImageUploadUrl : '/admin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
        filebrowserFlashUploadUrl : '/admin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
    });

			</script>
    </p>
	    <p>
	      <input name="page" type="hidden" id="page" value="<?PHP echo $FAL->page; ?>" />
          <input name="id" type="hidden" id="id" value="<?PHP echo $FAL->id; ?>" />
			<input type="submit" value="Save Changes" />
		</p>
</form>
</div>
</body>
</html>
